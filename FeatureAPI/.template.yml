cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - vendor/

# documentation using phpdox
.pages:
  image: php:7.3-apache
  before_script:
    - apt-get update -yqq
    - apt-get install wget libxslt-dev -yqq
    - docker-php-ext-install xsl
    - wget http://phpdox.de/releases/phpdox.phar
    - chmod +x phpdox.phar
    - mv phpdox.phar /usr/local/bin/phpdox
  script:
    - phpdox
    - cp ./public/index.xhtml ./public/index.html
  artifacts:
    paths:
      - public/

# testing using phpunit
.test:
  stage: test
  image: php:7.3-apache
  before_script:
    - apt-get update -yqq
    - apt-get install git wget libpng-dev libxslt-dev zlib1g-dev libzip-dev -yqq
    - pecl install xdebug-2.7.2
    - docker-php-ext-enable xdebug
    - echo "xdebug.remote_enable=1" >> /usr/local/etc/php/php.ini
    - docker-php-ext-install pdo_mysql && docker-php-ext-install curl && docker-php-ext-install json
    - docker-php-ext-install openssl && docker-php-ext-install libxml && docker-php-ext-install simplexml
    - docker-php-ext-install bcmath && docker-php-ext-install gd && docker-php-ext-install zip
    - wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
    - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    - php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    - php composer-setup.php
    - php -r "unlink('composer-setup.php'); unlink('installer.sig');"
    - php composer.phar install
  script:
    - ./vendor/bin/phpunit --bootstrap ./vendor/autoload.php --whitelist $SOURCE_LOCATION --coverage-text --colors=never $TEST_LOCATION
  cache:
    paths:
      - vendor/

# database migration using flyway
.database:
  image: php:7.3-apache
  stage: build
  before_script:
    - apt update -yq
    - apt install zip git wget libpng-dev libxslt-dev zlib1g-dev libzip-dev jq -yq
    - apt-get install mariadb-client -yq
    - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    - unzip awscliv2.zip
    - ./aws/install
    - export AWS_ACCESS_KEY_ID=$(echo -e "$AWS_S3_KEY" | base64 --decode)
    - export AWS_SECRET_ACCESS_KEY=$(echo -e "$AWS_S3_SECRET" | base64 --decode)
    - export AWS_DEFAULT_REGION=us-east-1
    - export AWS_SCRIPT_BUCKET=$(echo -e "$AWS_SCRIPT_BUCKET" | base64 --decode)
    - aws s3 cp ${AWS_SCRIPT_BUCKET}vault_env_exporter.sh .
    - chmod +x vault_env_exporter.sh
    - source ./vault_env_exporter.sh
    - '[ -z "$DEV_DATABASE_NAME" ] && (echo "DEV_DATABASE_NAME Unset" && exit 1)'
    - '[ -z "$DEV_SCHEMA_HISTORY" ] && (echo "DEV_SCHEMA_HISTORY Unset" && exit 1)'
    - '[ -z "$DATABASE_URL" ] && (echo "DATABASE_URL Unset" && exit 1)'
    - '[ -z "$DATABASE_PORT" ] && (echo "DATABASE_PORT Unset" && exit 1)'
    - '[ -z "$DATABASE_USER" ] && (echo "DATABASE_USER Unset" && exit 1)'
    - '[ -z "$DATABASE_PASS" ] && (echo "DATABASE_PASS Unset" && exit 1)'
    - export DATABASE_NAME=$DEV_DATABASE_NAME
    - export SCHEMA_HISTORY=$DEV_SCHEMA_HISTORY
    - mysql --user=$DATABASE_USER --password=$DATABASE_PASS --host=$DATABASE_URL --port=$DATABASE_PORT -Bse "CREATE DATABASE IF NOT EXISTS ${DATABASE_NAME}";
    - wget https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/5.1.4/flyway-commandline-5.1.4-linux-x64.tar.gz
    - tar -xzf flyway-commandline-5.1.4-linux-x64.tar.gz
    - rm flyway-commandline-5.1.4-linux-x64.tar.gz
    - mv database/*.sql flyway-5.1.4/sql/
    - cd flyway-5.1.4
    - echo "flyway.url=jdbc:mariadb://$DATABASE_URL:$DATABASE_PORT/$DATABASE_NAME" > conf/flyway.conf
    - echo "flyway.user=$DATABASE_USER" >> conf/flyway.conf
    - echo "flyway.password=$DATABASE_PASS" >> conf/flyway.conf
    - echo "flyway.baselineVersion=0" >> conf/flyway.conf
    - echo "flyway.baselineOnMigrate=true" >> conf/flyway.conf
    - echo "flyway.table=$SCHEMA_HISTORY" >> conf/flyway.conf
  script:
    - ./flyway migrate

## database migration using flyway
.database-flyway:
  image: php:7.3-apache
  stage: build
  before_script:
    - apt update -yq
    - apt install zip git wget libpng-dev libxslt-dev zlib1g-dev libzip-dev jq -yq
  script:
    - wget https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/6.0.0/flyway-commandline-6.0.0-linux-x64.tar.gz
    - tar -xzf flyway-commandline-6.0.0-linux-x64.tar.gz
    - rm flyway-commandline-6.0.0-linux-x64.tar.gz
    - mv database/*.sql flyway-6.0.0/sql/
    - cd flyway-6.0.0

## deploy application to heroku
.application-heroku:
  stage: deploy
  before_script:
    - echo "====> UPDATE RUNNER"
    - apt update -yq
    - apt install zip jq -yq
    - echo "====> GET ENV VARIABLES"
    - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    - unzip awscliv2.zip
    - ./aws/install
    - export AWS_ACCESS_KEY_ID=$(echo -e "$AWS_S3_KEY" | base64 --decode)
    - export AWS_SECRET_ACCESS_KEY=$(echo -e "$AWS_S3_SECRET" | base64 --decode)
    - export AWS_DEFAULT_REGION=us-east-1
    - export AWS_SCRIPT_BUCKET=$(echo -e "$AWS_SCRIPT_BUCKET" | base64 --decode)
    - aws s3 cp ${AWS_SCRIPT_BUCKET}vault_env_exporter.sh .
    - chmod +x vault_env_exporter.sh
    - source ./vault_env_exporter.sh
    - aws s3 cp ${AWS_SCRIPT_BUCKET}environment_replacer.sh .
    - chmod +x environment_replacer.sh
    - cp .env.example .env
    - ./environment_replacer.sh
    - cp .env.parsed .env
    - cat .env
    - echo "====> CREATE NGINX CONFIG"
    - rm -f nginx_heroku
    - echo "location / {" >> nginx_heroku.conf
    - echo "  try_files $uri @rewriteapp;" >> nginx_heroku.conf
    - echo "}" >> nginx_heroku.conf
    - echo "location @rewriteapp {" >> nginx_heroku.conf
    - echo "  rewrite ^(.*)$ /index.php/$1 last;" >> nginx_heroku.conf
    - echo "}" >> nginx_heroku.conf
    - echo "location ~ ^/index\.php(/|$) {" >> nginx_heroku.conf
    - echo "  try_files @heroku-fcgi @heroku-fcgi;" >> nginx_heroku.conf
    - echo "  internal;" >> nginx_heroku.conf
    - echo "}" >> nginx_heroku.conf
    - cat nginx_heroku.conf
    - echo "====> CREATE PROCFILE"
    - rm -f Procfile
    - "echo 'web: vendor/bin/heroku-php-nginx -C nginx_heroku.conf' >> Procfile"
    - python download.py
    - gem install dpl
  script:
    - dpl --provider=heroku --app=$HEROKU_APP_NAME --skip_cleanup=true --api-key=$HEROKU_API_KEY

# deploy to zettagrid
.application-zettagrid:
  stage: deploy
  before_script:
    - echo "====> UPDATE RUNNER"
    - apt update -yq
    - apt install zip jq wget rsync sshpass -yq
    - ls -al
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - chmod 600 ~/.ssh/config
    - echo "====> DOWNLOAD PHP PROJECT TEMPLATE"
    - python download.py
    - echo "====> GET ENV VARIABLES"
    - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    - unzip awscliv2.zip
    - ./aws/install
    - export AWS_ACCESS_KEY_ID=$(echo -e "$AWS_S3_KEY" | base64 --decode)
    - export AWS_SECRET_ACCESS_KEY=$(echo -e "$AWS_S3_SECRET" | base64 --decode)
    - export AWS_DEFAULT_REGION=us-east-1
    - export AWS_SCRIPT_BUCKET=$(echo -e "$AWS_SCRIPT_BUCKET" | base64 --decode)
    - aws s3 cp ${AWS_SCRIPT_BUCKET}vault_env_exporter.sh .
    - chmod +x vault_env_exporter.sh
    - source ./vault_env_exporter.sh
    - aws s3 cp ${AWS_SCRIPT_BUCKET}environment_replacer.sh .
    - chmod +x environment_replacer.sh
    - cp .env.example .env
    - ./environment_replacer.sh
    - mv .env.parsed .env
    - cat .env
    - echo "====> CREATE NGINX CONFIG"
    - echo "location /$FEATURE_NAME {" >> nginx_zettagrid.conf
    - echo "  try_files \$uri \$uri/ /$FEATURE_NAME/index.php;" >> nginx_zettagrid.conf
    - echo "}" >> nginx_zettagrid.conf
    - cat nginx_zettagrid.conf
    - rm vault_env_exporter.sh environment_replacer.sh .env.example awscliv2.zip
    - rm -r aws
    - ls -al
  script:
    - echo "====> DEPLOY TO $SSH_SERVER_URL"
    - sshpass -p $SSH_PASSWORD rsync -arvze "ssh -p $SSH_PORT" --rsync-path="mkdir -p $ROOT_FOLDER/$APP_FOLDER/$FEATURE_NAME && rsync" --progress --delete ./ $SSH_USERNAME@$SSH_SERVER_URL:$ROOT_FOLDER/$APP_FOLDER/$FEATURE_NAME
    - echo "====> INSTALL COMPOSER"
    - sshpass -p $SSH_PASSWORD ssh $SSH_USERNAME@$SSH_SERVER_URL -p $SSH_PORT "cd $ROOT_FOLDER/$APP_FOLDER/$FEATURE_NAME && composer install"
    - echo "====> RELOAD NGINX"
    - sshpass -p $SSH_PASSWORD ssh $SSH_USERNAME@$SSH_SERVER_URL -p $SSH_PORT "sudo systemctl reload nginx && sudo systemctl status nginx"

# deploy application to aws
.application:
  stage: deploy
  before_script:
    - apt update -yq
    - apt install zip jq -yq
    - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    - unzip awscliv2.zip
    - ./aws/install
    - export AWS_ACCESS_KEY_ID=$(echo -e "$AWS_S3_KEY" | base64 --decode)
    - export AWS_SECRET_ACCESS_KEY=$(echo -e "$AWS_S3_SECRET" | base64 --decode)
    - export AWS_DEFAULT_REGION=us-east-1
    - export AWS_SCRIPT_BUCKET=$(echo -e "$AWS_SCRIPT_BUCKET" | base64 --decode)
    - aws s3 cp ${AWS_SCRIPT_BUCKET}vault_env_exporter.sh .
    - chmod +x vault_env_exporter.sh
    - source ./vault_env_exporter.sh
    - aws s3 cp ${AWS_SCRIPT_BUCKET}environment_replacer.sh .
    - chmod +x environment_replacer.sh
    - cp .env.example .env
    - ./environment_replacer.sh
    - (echo -e "$SSH_KEY" | base64 --decode) > technology.pem
    - chmod 400 technology.pem
    - mkdir -p ~/.ssh
    - chmod 600 ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - export WORKSPACE=$(echo $FEATURE_NAME | sed -e 's#/#-#g')
    - '[ ! -z "$SCHEDULER" ] && (export SERVER_URL=$SCHEDULER_SERVER_URL)'
    - '[ -z "$FEATURE_NAME" ] && (echo "FEATURE_NAME Unset" && exit 1)'
    - '[ -z "$SERVER_URL" ] && (echo "SERVER_URL Unset" && exit 1)'
  script:
    - echo "====> Deploying to $SERVER_URL"
    - ssh -i "technology.pem" ubuntu@$SERVER_URL "./setup.sh $FEATURE_NAME $GIT_BRANCH $CI_PROJECT_PATH $CI_COMMIT_TAG"
    - scp -i "technology.pem" .env.parsed  ubuntu@$SERVER_URL:~/cicd/$WORKSPACE/.env
